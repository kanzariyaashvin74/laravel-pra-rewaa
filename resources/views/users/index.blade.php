@extends('layouts.master')
@section('content')
<div class="row pt-2">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">User's List</h3>
                <div class="card-tools">
                  <ul class="nav nav-pills ml-auto">
                    <li class="nav-item">
                      <a class="nav-link active add-user" href="javascript:void(0);" data-toggle="modal" data-target="#modal-lg"><i class="fas fa-plus"> Add User</i></a>
                    </li>
                  </ul>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="table-responsive">
                    <table id="userdatatable" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                         
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
<!-- Modal Box Start -->
<div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Update User</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <form action="javascript:void(0)" method="POST" id="user-update-form">
                @csrf
                <input type="hidden" name="_method" id="form_method" value="PUT">
                <input type="hidden" name="updateId" id="updateId">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name">First Name</label>
                                <input type="text" class="form-control firstname_error_input" id="firstname" name="firstname" autocomplete="off" placeholder="Enter FirstName" value="">
                                <span class="invalid-feedback d-block" role="alert">
                                    <strong class="firstname_error"></strong>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name">Last Name</label>
                                <input type="text" class="form-control lastname_error_input" id="lastname" name="lastname" autocomplete="off" placeholder="Enter LastName" value="">
                                <span class="invalid-feedback d-block" role="alert">
                                    <strong class="lastname_error"></strong>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name">User Name</label>
                                <input type="text" class="form-control username_error_input" id="username" name="username" autocomplete="off" placeholder="Enter UserName" value="">
                                <span class="invalid-feedback d-block" role="alert">
                                    <strong class="username_error"></strong>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control email_error_input" id="email" name="email" autocomplete="off" placeholder="Enter Email" value="">
                                <span class="invalid-feedback d-block" role="alert">
                                    <strong class="email_error"></strong>
                                </span>
                             
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary update-btn">Update</button>
                </div>
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
<!-- End Modal box -->
@endsection