@extends('layouts.app')

@section('content')
<div class="register-box">
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="{{ url('/') }}" class="h1"><b>Register</a>
    </div>
    <div class="card-body">
      <!-- <p class="login-box-msg">Register a new membership</p> -->

      <form action="{{ route('register') }}" method="POST" id="registration-form">
      @csrf
        <div class="input-group">
          <input type="text" id="name" class="form-control @error('name') is-invalid @enderror" placeholder="Enter Your Name" name="name" value="{{ old('name') }}" autocomplete="name" autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
          @error('name')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        <div class="input-group mt-3">
          <input type="email" id="email" class="form-control @error('email') is-invalid @enderror" placeholder=" Enter Your Email" name="email" value="{{ old('email') }}" autocomplete="email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          @error('email')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        <div class="input-group mt-3">
          <input type="password" id="password" class="form-control @error('password') is-invalid @enderror" placeholder=" Enter Your Password" name="password"  autocomplete="new-password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          @error('password')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        <div class="input-group mt-3">
          <input type="password" id="password_confirm" class="form-control" placeholder="confirm password" name="password_confirmation" autocomplete="new-password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>

        </div>
          <div class="social-auth-links text-center">
            <button type="submit" class="btn btn-block btn-success"> <i class="far fa-registered mr-2"></i> Register </button>
         </div>
      </form>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->
@endsection
