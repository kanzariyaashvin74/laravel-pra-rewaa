<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Users\UsersController;
use App\Models\UserData;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(Auth::check())
    {
        return redirect('/home');
    }
    else
    {
        return redirect('/login');
    }
});

# Run migration.
Route::get('/laravel-migration', function() {
    Artisan::call('migrate');
    return "<h1 style='color:blue;'> Migration Done Successfully...</h1>";
});

# Run DB - Seeder .
Route::get('/laravel-seed', function() {
    Artisan::call('db:seed');
    return "<h1 style='color:blue;'> Record added Successfully...</h1>";
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('users', UsersController::class);
Route::post('users/destroy/{id}',[UsersController::class,'destroy']);

