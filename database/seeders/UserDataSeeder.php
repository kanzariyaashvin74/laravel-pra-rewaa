<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class UserDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach(range(1, 2000) as $index)
        {
            DB::table('userdata')->insert([
                'firstname' => $faker->firstName(10),
                'lastname'  => $faker->lastName(12),
                'username'  => $faker->userName(12),
                'email'     => $faker->unique()->email(8),
            ]);
        }
    }
}
