<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserData;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = UserData::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){

                        $btn = '<a href="javascript:void(0)" data-id="'.$row->id.'" data-toggle="modal" data-target="#modal-lg" class="edit btn btn-primary btn-sm edit-btn">Edit</a>';

                        $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleted-record">Delete</a>';

                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname'  => 'required',
            'lastname'   => 'required',
            'username'   => 'required',
            'email'      => 'required|email|unique:userdata,email',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => '0', 'errors' => $validator->errors()->toArray()]);
        }
        else
        {
            UserData::create($request->all());
            return response()->json(['status' => '1', 'success' =>'user Added successfully']);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(UserData $user)
    {
        return response()->json([
            'status' => 1,
            'data' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserData $user)
    {
        $validator = Validator::make($request->all(), [
            'firstname'  => 'required',
            'lastname'   => 'required',
            'username'   => 'required',
            'email' => 'required|email|unique:userdata,email,'.$user->id,
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => '0', 'errors' => $validator->errors()->toArray()]);
        }
        else
        {
            $user->update($request->all());
            return response()->json(['status' => '1', 'success' =>'user update successfully']);

        }
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        UserData::destroy($id);

        return response()->json([
            'status' => 1
        ]);
    }
}
