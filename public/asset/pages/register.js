$(document).ready(function(){

    /*  From Validation */
    $('#registration-form').validate({ 
        errorClass: "error-class",
        validClass: "valid-class",
        errorElement: 'div',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        onError : function(){
            $('.input-group.error-class').find('.help-block.form-error').each(function() {
            $(this).closest('.form-group').addClass('error-class').append($(this));
            });
        },
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength:8
            }, 
            password_confirmation: {
                required: true,
                minlength:8,
                equalTo:"#password"
            },
            
        },
        messages: {
            name: {
                required: "Please enter name.",
            },      
            email: {
                required: "Please enter email.",
                email: "Please enter valid email address."
            },      
            password: {
                required: "Please enter password.",
                minlength:"Your password must be at least 8 characters."
            }, 
            password_confirmation: {
                required: "Please enter password.",
                minlength:"Your password must be at least 8 characters.",
                equalTo: "Plese enter the same password as above.",
            },    
        },
    });


});