$(document).ready(function () {
    /* Toster Time */ 
    var Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });

    /*  Fetch Data and showing table */
    var dataTable = $('#userdatatable').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        pageLength: 10,
        // scrollX: true,
        "order": [[0, "desc"]],
        ajax: base_url + '/users',
        columns: [
            { data: 'firstname', name: 'firstname' },
            { data: 'lastname', name: 'lastname' },
            { data: 'username', name: 'username' },
            { data: 'email', name: 'email' },
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ]

    });

    // Edit Record for selected User.
    $(document).on('click', '.add-user', function (e) {
        $('#user-update-form')[0].reset();
        $('.modal-title').text('Add User');
        $('.update-btn').text('Save');
        $('#form_method').val('post');
    });
   
    // Edit Record for selected User.
    $(document).on('click', '.edit-btn', function (e) {
        var userId = $(this).attr('data-id');
        $('#user-update-form')[0].reset();
        $('.modal-title').text('Update User');
        $('.update-btn').text('Update');
        $('#form_method').val('PUT');
        $.ajax({
            method: 'GET',
            url: base_url + '/users/' + userId + '/edit',
            dataType: 'json',
            success: function (data) {
                if (data.status == 1) {
                    $('#updateId').val(data.data.id);
                    $('#firstname').val(data.data.firstname);
                    $('#lastname').val(data.data.lastname);
                    $('#username').val(data.data.username);
                    $('#email').val(data.data.email);
                }
            }
        });

    });

    /* Add and Update record for UserData module */
    $('#user-update-form').on('submit', function (event) {
        event.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let formData = new FormData($('#user-update-form')[0]);
        var userId = $('#updateId').val();
        
        if(userId !== '')
        {
            /*  User Updated Records */
            $.ajax({
                method: 'POST',
                url: base_url + '/users/' + userId,
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status == 1) {
                        $("#modal-lg").modal('hide');
                        $('#user-update-form')[0].reset();
                        $('#userdatatable').DataTable().ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'User updated successfully.'
                        })
                    }
                    else
                    {
                        $.each(data.errors, function(prefix, val){
                            $('.'+prefix+'_error_input').addClass('is-invalid');
                            $('.'+prefix+'_error').html(val[0]);
                        });
                    }
                }
            });
        }
        else
        {
            /*  User Added a new Records */
          
            $.ajax({
                type: 'POST',
                url: base_url + '/users',
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.status == 1) {
                        $("#modal-lg").modal('hide');
                        $('#user-update-form')[0].reset();
                        $('#userdatatable').DataTable().ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'User Added successfully.'
                        })
                    }
                    else
                    {
                        $.each(data.errors, function(prefix, val){
                            $('.'+prefix+'_error_input').addClass('is-invalid');
                            $('.'+prefix+'_error').html(val[0]);
                        });
                    }
                }
            });
        }
    });


    /* Delete Record */
    $(document).on('click', '.deleted-record', function (event) {
        var userId = $(this).attr('data-id');
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't to delete this record!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: 'POST',
                    url: base_url + '/users/destroy/' + userId,
                    dataType: 'json',
                    success: function (data) {
                        if (data.status == 1) {
                            $('#userdatatable').DataTable().ajax.reload();

                            Toast.fire({
                                icon: 'success',
                                title: 'User Deleted successfully.'
                            })
                        }
                        else
                        {
                            $('#userdatatable').DataTable().ajax.reload();
                            Toast.fire({
                                icon: 'error',
                                title: 'opp, somthing want wrong!'
                            })
                        }
                    }
                });
            }
        })
    });

    /* Modal Box Hide events */
    $('#modal-lg').on('hidden.bs.modal', function () {
        $('#user-update-form')[0].reset();
        
        // Errors msg hide.
        $('.firstname_error_input').removeClass('is-invalid');
        $('.lastname_error_input').removeClass('is-invalid');
        $('.username_error_input').removeClass('is-invalid');
        $('.email_error_input').removeClass('is-invalid');
        
        // Remove error Content.
        $('.firstname_error').html('');
        $('.lastname_error').html('');
        $('.username_error').html('');
        $('.email_error').html('');
    })
});